package Breakout;

/**
 * Representation of a ball in a two dimensional space.
 * 
 * @author Lukas Hass, Phillip Kelsch
 *
 */
public class Ball {

	// balls position
	private int x;
	private int y;

	// ball is a square
	private int size;

	// components of a 2d vector
	// representing the balls direction
	private int speedX;
	private int speedY;

	/**
	 * Creates a new Ball.
	 * 
	 * @param size
	 *            The size of the new ball.
	 */
	public Ball(int size) {
		this.size = size;
	}

	////////////////////
	// getter
	////////////////////

	/**
	 * @return The balls horizontal position.
	 */
	public int getX() {
		return x;
	}

	/**
	 * @return The balls vertical position.
	 */
	public int getY() {
		return y;
	}

	/**
	 * @return The balls size.
	 */
	public int getSize() {
		return size;
	}

	/**
	 * @return The balls {@code size-1}.
	 */
	public int getOffset() {
		return size - 1;
	}

	/**
	 * @return The balls horizontal speed.
	 */
	public int getSpeedX() {
		return speedX;
	}

	/**
	 * @return The balls vertical speed.
	 */
	public int getSpeedY() {
		return speedY;
	}

	////////////////////
	// setter
	////////////////////

	/**
	 * Set balls horizontal position.
	 * 
	 * @param x
	 *            The new position.
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Set balls vertical position.
	 * 
	 * @param y
	 *            The new position.
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * Set the balls size.
	 * 
	 * @param size
	 *            New size.
	 */
	public void setSize(int size) {
		this.size = size;
	}

	/**
	 * Set balls horizontal speed.
	 * 
	 * @param speed
	 *            New speed.
	 */
	public void setSpeedX(int speed) {
		this.speedX = speed;
	}

	/**
	 * Set balls vertical speed.
	 * 
	 * @param speed
	 *            New speed.
	 */
	public void setSpeedY(int speed) {
		this.speedY = speed;
	}
}
