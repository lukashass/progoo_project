package Breakout;

import java.awt.Color;
import java.io.IOException;

import de.cau.infprogoo.lighthouse.LighthouseDisplay;

/**
 * Implementation of a view that shows the model on the lighthouse.
 * 
 * @author Lukas Hass, Phillip Kelsch
 *
 */
public class ViewLighthouse extends BreakoutView {

	private static final Color[] COLORS = { Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN, Color.CYAN };

	LighthouseDisplay display;
	byte[] data;

	/**
	 * Creates a new view.
	 * 
	 * @param user
	 *            User for authentication with the lighthouse API.
	 * @param token
	 *            Token for the lighthouse API.
	 */
	public ViewLighthouse(String user, String token) {
		display = connect(user, token);

		// This array contains for every window (14 rows, 28 columns) three
		// bytes that define the red, green, and blue component of the color
		// to be shown in that window.
		data = new byte[14 * 28 * 3];
	}

	/**
	 * Connects to the lighthouse API.
	 * 
	 * @param user
	 *            User for authentication with the lighthouse API.
	 * @param token
	 *            Token for the lighthouse API.
	 * @return The active connection with the lighthouse API.
	 */
	public LighthouseDisplay connect(String user, String token) {

		LighthouseDisplay result = new LighthouseDisplay(user, token, 2);

		// Try connecting to the display
		try {
			result.connect();

			// wait after connecting so the data sent can be received by the lighthouse.
			Thread.sleep(400);

		} catch (Exception e) {
			System.out.println("Connection failed: " + e.getMessage());
			e.printStackTrace();
		}

		return result;
	}

	@Override
	public void createView(BreakoutModel model) {

		Ball ball = model.getBall();
		Paddle paddle = model.getPaddle();
		Brick[][] bricks = model.getBricks();
		
		// draw ball
		drawPixel(ball.getX(), ball.getY(), Color.BLUE);

		// draw paddle
		drawWidth(paddle.getX(), paddle.getY(), paddle.getWidth(), Color.RED);

		// draw bricks
		for (int i = 0; i < bricks.length; i++) {
			for (int j = 0; j < bricks[i].length; j++) {
				if (bricks[i][j].getActive()) {

					drawWidth(bricks[i][j].getX(), bricks[i][j].getY(), bricks[i][j].getWidth(),
							COLORS[j % COLORS.length]);

				}
			}
		}

		// Send data to the display
		try {
			display.send(data);
		} catch (IOException e) {
			System.out.println("Connection failed: " + e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public void clearView(int x, int y) {

		// reset the data array to black (byte is initialized with 0)
		data = new byte[14 * 28 * 3];

	}

	/**
	 * Paints a given pixel in the data array.
	 * 
	 * @param x
	 *            Horizontal position of the pixel.
	 * @param y
	 *            Vertical position of the pixel.
	 * @param c
	 *            Color to paint the pixel with.
	 */
	private void drawPixel(int x, int y, Color c) {

		data[3 * (28 * y + x)] = (byte) c.getRed();
		data[3 * (28 * y + x) + 1] = (byte) c.getGreen();
		data[3 * (28 * y + x) + 2] = (byte) c.getBlue();

	}

	/**
	 * Paints multiple pixels next to each other.
	 * 
	 * @param x
	 *            Horizontal position of the pixel.
	 * @param y
	 *            Vertical position of the pixel.
	 * @param width
	 *            Number of pixels to paint.
	 * @param c
	 *            Color to paint the pixels with.
	 */
	private void drawWidth(int x, int y, int width, Color c) {
		for (int i = 0; i < width; i++) {
			drawPixel(x + i, y, c);
		}
	}
}
