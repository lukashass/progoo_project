package Breakout;

/**
 * 
 * Representation of a brick in two dimensional space.
 * 
 * @author Lukas Hass, Phillip Kelsch
 *
 */
public class Brick {

	private int x;
	private int y;
	private int width;
	private int height;
	private boolean alive;

	/**
	 * Creates a new Brick.
	 * 
	 * @param x
	 *            horizontal coordinate of the brick.
	 * @param y
	 *            vertical coordinate of the brick.
	 * @param width
	 *            width of the brick.
	 * @param height
	 *            height of the brick.
	 */
	public Brick(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		alive = true;
	}

	////////////////////
	// getter setter
	////////////////////

	/**
	 * @return horizontal coordinate.
	 */
	public int getX() {
		return x;
	}

	/**
	 * Sets the horizontal coordinate.
	 * 
	 * @param x
	 *            horizontal coordinate.
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return vertical coordinate.
	 */
	public int getY() {
		return y;
	}

	/**
	 * Sets the vertical coordinate.
	 * 
	 * @param y
	 *            vertical coordinate.
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * @return width of the brick.
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Sets the width of the brick.
	 * 
	 * @param width
	 *            width of the brick.
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * @return height of the brick.
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Sets the height of the brick.
	 * 
	 * @param height
	 *            height of the brick.
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * Returns whether the brick is active or not.
	 * 
	 * @return {@code true} when brick is active or {@code false} when brick is
	 *         dead.
	 */
	public boolean getActive() {
		return alive;
	}

	/**
	 * Sets the state of a brick to dead(not alive).
	 */
	public void destroy() {
		alive = false;

	}
}
