package Breakout;

import java.util.ArrayList;

/**
 * Implementation of a model of the game Breakout.
 * 
 * @author Lukas Hass, Phillip Kelsch
 *
 */
public class BreakoutModel {

	// list of all view showing this model
	private ArrayList<BreakoutView> views;

	// game elements
	private Ball ball;
	private Brick[][] bricks;
	private Paddle paddle;

	// game field bounds
	private int x;
	private int y;

	// brick properties
	private int brickRows;
	private int brickColumns;
	private int brickOffset;
	private int brickHeight;

	// balls start position
	private int ballStartX;
	private int ballStartY;

	// delay between moves of the balls and updates of the views
	private int gameSpeed;

	// state of the game
	private boolean gameOver;

	// keeps track of players score
	private int score;

	// helper
	boolean flat;

	/**
	 * Creates a new model.
	 * 
	 * @param x
	 *            Horizontal bounds of game.
	 * @param y
	 *            Vertical bounds of game.
	 * @param gameSpeed
	 *            Delay between ball steps and view updates.
	 * @param ballStartX
	 *            Horizontal start position of ball.
	 * @param ballStartY
	 *            Vertical start position of ball.
	 * @param ballSize
	 *            Size of ball.
	 * @param paddleWidth
	 *            Width of the paddle.
	 * @param paddleHeight
	 *            Height of the paddle.
	 * @param brickColumns
	 *            Number of bricks next to each other.
	 * @param brickRows
	 *            Number of bricks on top of each other.
	 * @param brickOffset
	 *            Number of empty rows at the of the game.
	 * @param brickHeight
	 *            Height of the bricks.
	 */
	public BreakoutModel(int x, int y, int gameSpeed, int ballStartX, int ballStartY, int ballSize, int paddleWidth,
			int paddleHeight, int brickColumns, int brickRows, int brickOffset, int brickHeight) {

		this.x = x;
		this.y = y;
		this.gameSpeed = gameSpeed;
		this.brickColumns = brickColumns;
		this.brickRows = brickRows;
		this.brickOffset = brickOffset;
		this.brickHeight = brickHeight;
		this.ballStartX = ballStartX;
		this.ballStartY = ballStartY;

		ball = new Ball(ballSize);

		// place paddle in the middle of the floor
		paddle = new Paddle(x / 2, y - paddleHeight, paddleWidth, paddleHeight);

		views = new ArrayList<BreakoutView>();

		init();
	}

	/**
	 * Calls update(this) on every view to reconstruct their displays
	 */
	private void notifyViews() {
		for (BreakoutView view : views) {
			view.update(this);
		}
	}

	/**
	 * Set initial game state.
	 */
	private void init() {

		gameOver = true;
		score = 0;
		bricks = generateBricks();
		ball.setX(ballStartX);
		ball.setY(ballStartY);
		ball.setSpeedX(1);
		ball.setSpeedY(1);

	}

	////////////////////
	// getter
	////////////////////

	/**
	 * @return Width of the model.
	 */
	public int getX() {
		return x;
	}

	/**
	 * @return Height of the model.
	 */
	public int getY() {
		return y;
	}

	/**
	 * @return Ball of the model.
	 */
	public Ball getBall() {
		return ball;
	}

	/**
	 * @return Bricks of the model.
	 */
	public Brick[][] getBricks() {
		return bricks;
	}

	/**
	 * @return Paddle of the model.
	 */
	public Paddle getPaddle() {
		return paddle;
	}

	/**
	 * @return The players score.
	 */
	public int getScore() {
		return score;
	}

	/**
	 * @return {@code true} when the game is lost, {@code false} when not.
	 */
	public boolean isGameOver() {
		return gameOver;
	}

	////////////////////
	// setter
	////////////////////

	/**
	 * Sets the players score.
	 * 
	 * @param score
	 *            New score.
	 */
	public void setScore(int score) {
		this.score = score;
	}

	/**
	 * Sets the state of the game.
	 * 
	 * @param gameOver
	 *            State of the game.
	 */
	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}

	/**
	 * Add a view showing the model.
	 * 
	 * @param view
	 *            View to add.
	 */
	public void addView(BreakoutView view) {
		views.add(view);
	}

	/**
	 * Runs until the program is closed. Calls {@code step()} every
	 * {@code gameSpeed} milliseconds.
	 */
	public void gameLoop() {

		// show the game before it starts
		notifyViews();

		long timestamp = 0;

		while (true) {
			// only play when the game isn't lost
			if (!gameOver) {

				// only step and update every gameSpeed milliseconds
				if (System.currentTimeMillis() - gameSpeed > timestamp) {
					timestamp = System.currentTimeMillis();
					step();
					notifyViews();
				}
			}

			// gives java time to do stuff
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Sets the new position of the ball.
	 */
	private void step() {

		flat = false;

		// check at least once for collision
		// after colliding check again
		boolean change;
		do {
			change = checkCollision();
		} while (change);

		if (!isGameOver()) {
			ball.setX(ball.getX() + ball.getSpeedX());
			ball.setY(ball.getY() + ball.getSpeedY());
		}
	}

	/**
	 * Checks if the ball collides with anything and change its direction
	 * accordingly.
	 * 
	 * @return {@code true} when ball collided, {@code false} when not.
	 */
	private boolean checkCollision() {
		boolean collided = false;

		int newX = ball.getX() + ball.getSpeedX();
		int newY = ball.getY() + ball.getSpeedY();

		// collision with floor
		if (newY + ball.getOffset() >= this.y) {
			init();
			return false;
		}
		// collision with ceiling
		if (newY < 0) {
			ball.setSpeedY(ball.getSpeedY() * -1);
			collided = true;
		}

		// collision with walls
		if (newX < 0 || newX + ball.getOffset() >= this.x) {
			ball.setSpeedX(ball.getSpeedX() * -1);
			collided = true;
		}

		/////////////////////////
		// collision with bricks
		/////////////////////////

		// upper left corner of ball
		boolean brickChange = brickCollision(newX, newY, ball.getX(), ball.getY());

		// lower right corner of ball
		if (!brickChange) {
			brickChange = brickCollision(newX + ball.getOffset(), newY + ball.getOffset(),
					ball.getX() + ball.getOffset(), ball.getY() + ball.getOffset());
		}

		// upper right corner of ball
		if (!brickChange) {
			brickChange = brickCollision(newX + ball.getOffset(), newY, ball.getX() + ball.getOffset(), ball.getY());
		}

		// lower left corner of ball
		if (!brickChange) {
			brickChange = brickCollision(newX, newY + ball.getOffset(), ball.getX(), ball.getY() + ball.getOffset());
		}

		// when collided with brick set change
		if (brickChange) {
			collided = true;
		}

		/////////////////////////
		// collision with paddle
		/////////////////////////

		// first check if paddle is right underneath the ball
		// flat makes sure this happens only once per step

		// check lower left corner
		if (!flat && ball.getX() < paddle.getX() + paddle.getWidth() && ball.getX() >= paddle.getX()
				&& ball.getY() + ball.getOffset() + 1 < paddle.getY() + paddle.getHeight()
				&& ball.getY() + ball.getOffset() + 1 >= paddle.getY()) {
			ball.setSpeedY(ball.getSpeedY() * -1);
			collided = true;
			flat = true;
		} else

		// check lower right corner
		if (!flat && ball.getX() + ball.getOffset() < paddle.getX() + paddle.getWidth()
				&& ball.getX() + ball.getOffset() >= paddle.getX()
				&& ball.getY() + ball.getOffset() + 1 < paddle.getY() + paddle.getHeight()
				&& ball.getY() + ball.getOffset() + 1 >= paddle.getY()) {
			ball.setSpeedY(ball.getSpeedY() * -1);
			collided = true;
			flat = true;
		} else

		// check for diagonal collision and fully reverse the ball

		// check lower left corner
		if (newX < paddle.getX() + paddle.getWidth() && newX >= paddle.getX()
				&& newY + ball.getOffset() < paddle.getY() + paddle.getHeight()
				&& newY + ball.getOffset() >= paddle.getY()) {
			ball.setSpeedX(ball.getSpeedX() * -1);
			ball.setSpeedY(ball.getSpeedY() * -1);
			collided = true;
		} else

		// check lower right corner
		if (newX + ball.getOffset() < paddle.getX() + paddle.getWidth() && newX + ball.getOffset() >= paddle.getX()
				&& newY + ball.getOffset() < paddle.getY() + paddle.getHeight()
				&& newY + ball.getOffset() >= paddle.getY()) {
			ball.setSpeedX(ball.getSpeedX() * -1);
			ball.setSpeedY(ball.getSpeedY() * -1);
			collided = true;
		}

		return collided;
	}

	/**
	 * Checks if a block contains given coordinates.
	 * 
	 * @param x
	 *            Horizontal coordinate.
	 * @param y
	 *            Vertical coordinate.
	 * @return The brick containing the coordinates, {@code null} when no brick is
	 *         found.
	 */
	private Brick brickContaining(int x, int y) {
		for (int i = 0; i < bricks.length; i++) {
			for (int j = 0; j < bricks[i].length; j++) {
				if (bricks[i][j].getActive() && x < bricks[i][j].getX() + bricks[i][j].getWidth()
						&& x >= bricks[i][j].getX() && y < bricks[i][j].getY() + bricks[i][j].getHeight()
						&& y >= bricks[i][j].getY()) {
					return bricks[i][j];
				}
			}
		}
		return null;
	}

	/**
	 * Creates a full width brick wall.
	 * 
	 * @return The array containing all the generated bricks.
	 */
	private Brick[][] generateBricks() {
		Brick[][] bricks = new Brick[brickColumns][brickRows];
		for (int i = 0; i < brickColumns; i++) {
			for (int j = 0; j < brickRows; j++) {
				bricks[i][j] = new Brick(i * x / brickColumns, j * brickHeight + brickOffset, x / brickColumns,
						brickHeight);
			}
		}
		return bricks;
	}

	/**
	 * Checks if a corner of the ball collides with a brick and change its direction
	 * accordingly.
	 * 
	 * @param newX
	 *            New horizontal coordinate of balls corner.
	 * @param newY
	 *            New vertical coordinate of balls corner.
	 * @param ballX
	 *            Current horizontal coordinate of balls corner.
	 * @param ballY
	 *            Current vertical coordinate of balls corner.
	 * @return {@code true} when collided, {@code false} when not.
	 */
	public boolean brickCollision(int newX, int newY, int ballX, int ballY) {

		boolean change = false;

		Brick brickNewX = brickContaining(newX, ballY);
		Brick brickNewY = brickContaining(ballX, newY);
		Brick brickNewBoth = brickContaining(newX, newY);

		// collided with two bricks simultaneously
		if (brickNewX != null && brickNewY != null) {
			ball.setSpeedX(ball.getSpeedX() * -1);
			ball.setSpeedY(ball.getSpeedY() * -1);
			brickNewX.destroy();
			brickNewY.destroy();
			score += 2;
			change = true;
		} else

		// collided with a brick in horizontal direction
		if (brickNewX != null) {
			ball.setSpeedX(ball.getSpeedX() * -1);
			brickNewX.destroy();
			score++;
			change = true;
		} else

		// collided with a brick in vertical direction
		if (brickNewY != null) {
			ball.setSpeedY(ball.getSpeedY() * -1);
			brickNewY.destroy();
			score++;
			change = true;
		} else

		// collided diagonally
		if (brickNewBoth != null) {
			ball.setSpeedX(ball.getSpeedX() * -1);
			ball.setSpeedY(ball.getSpeedY() * -1);
			brickNewBoth.destroy();
			score++;
			change = true;
		}

		return change;
	}

}