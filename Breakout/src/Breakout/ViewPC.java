package Breakout;

import java.awt.Color;
import java.awt.Font;

import acm.graphics.GCompound;
import acm.graphics.GLabel;
import acm.graphics.GOval;
import acm.graphics.GRect;

/**
 * Implementation of a view showing the model in a {@link GCompound}.
 * 
 * @author Lukas Hass, Phillip Kelsch
 *
 */
public class ViewPC extends BreakoutView {

	private static final Color[] COLORS = { Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN, Color.CYAN };
	private GRect background;
	private GCompound gameField;
	private int scale;

	/**
	 * Creates a new view.
	 * 
	 * @param scale
	 *            Multiplier to scale up the displayed model elements.
	 */
	public ViewPC(int scale) {
		this.scale = scale;
		gameField = new GCompound();
	}

	@Override
	public void createView(BreakoutModel model) {

		Ball ball = model.getBall();
		Paddle paddle = model.getPaddle();
		Brick[][] bricks = model.getBricks();

		// loop all bricks and draw those that aren't destroyed yet
		for (int i = 0; i < bricks.length; i++) {
			for (int j = 0; j < bricks[i].length; j++) {

				if (bricks[i][j].getActive()) {
					GRect brickRect = new GRect(bricks[i][j].getX() * scale, bricks[i][j].getY() * scale,
							bricks[i][j].getWidth() * scale, bricks[i][j].getHeight() * scale);

					brickRect.setFillColor(COLORS[j % COLORS.length]);
					brickRect.setFilled(true);
					gameField.add(brickRect);
				}
			}

		}

		// draw the paddle
		GRect paddleRect = new GRect(paddle.getX() * scale, paddle.getY() * scale, paddle.getWidth() * scale,
				paddle.getHeight() * scale);
		paddleRect.setFillColor(Color.LIGHT_GRAY);
		paddleRect.setFilled(true);
		gameField.add(paddleRect);

		// draw the ball
		GOval ballRect = new GOval(ball.getX() * scale, ball.getY() * scale, ball.getSize() * scale,
				ball.getSize() * scale);
		ballRect.setColor(Color.LIGHT_GRAY);
		ballRect.setFillColor(Color.LIGHT_GRAY);
		ballRect.setFilled(true);
		gameField.add(ballRect);

		// display the players score
		GLabel scoreLabel = new GLabel("Score: " + model.getScore());
		scoreLabel.setFont(new Font("TimesRoman", Font.PLAIN, 20));
		// position top right corner
		scoreLabel.setLocation(background.getWidth() - scoreLabel.getWidth(), 20);
		scoreLabel.setColor(Color.WHITE);
		gameField.add(scoreLabel);

		if (model.isGameOver()) {

			GLabel overLabel = new GLabel("Click to play!");
			overLabel.setFont(new Font("TimesRoman", Font.PLAIN, 50));
			overLabel.setLocation(0, background.getHeight() - overLabel.getHeight() / 2);
			overLabel.setColor(Color.WHITE);
			gameField.add(overLabel);

		}

	}

	@Override
	public void clearView(int x, int y) {

		gameField.removeAll();

		// create a new background
		if (background == null) {
			background = new GRect(x * scale, y * scale);
			background.setFillColor(Color.BLACK);
			background.setFilled(true);
		}
		gameField.add(background);
	}

	/**
	 * @return The {@link GCompound} containing all drawn game elements.
	 */
	public GCompound getGameField() {
		return gameField;
	}

}
