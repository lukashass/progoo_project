package Breakout;

/**
 * Representation of the paddel in two dimensional space.
 * 
 * @author Lukas Hass, Phillip Kelsch
 *
 */
public class Paddle extends Brick {

	/**
	 * 
	 * Creates a new paddle.
	 * 
	 * @param x
	 *            horizontal coordinate of the paddle.
	 * @param y
	 *            vertical coordinate of the paddle.
	 * @param width
	 *            width of the paddle.
	 * @param height
	 *            height of the paddle.
	 */
	public Paddle(int x, int y, int width, int height) {
		super(x, y, width, height);
	}

	/**
	 * Sets the horizontal coordinate of the paddle.
	 * 
	 * @param x
	 *            horizontal coordinate of the paddle.
	 * @param boundX
	 *            limits the horizontal bound that the paddle shall not surpass.
	 */
	public void setX(int x, int boundX) {
		if (x >= 0 && x + super.getWidth() <= boundX) {
			super.setX(x);
		}
	}

}
