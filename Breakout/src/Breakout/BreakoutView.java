package Breakout;

/**
 * Skeleton for a view.
 * 
 * @author Lukas Hass, Phillip Kelsch
 *
 */
public abstract class BreakoutView {

	/**
	 * Draws the view.
	 * 
	 * @param model The model to show.
	 */
	public abstract void createView(BreakoutModel model);

	/**
	 * Clears the view for next frame.
	 * 
	 * @param x
	 *            Width of the model shown the view.
	 * @param y
	 *            Height of the model shown the view.
	 */
	public abstract void clearView(int x, int y);

	/**
	 * Updates the view.
	 * 
	 * @param model
	 *            The model shown in the view.
	 */
	public void update(BreakoutModel model) {
		clearView(model.getX(), model.getY());
		createView(model);
	}
}
