import java.awt.event.MouseEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import Breakout.BreakoutModel;
import Breakout.ViewLighthouse;
import Breakout.ViewPC;
import acm.program.GraphicsProgram;

/**
 * Implementation of Breakout using the MVC pattern.
 * 
 * @author Lukas Hass, Phillip Kelsch
 *
 */
public class Breakout extends GraphicsProgram {

	// game settings
	private int boundX;
	private int boundY;

	private int scale;

	private int gameSpeed;

	private int brickRows;
	private int brickColumns;
	private int brickOffset;
	private int brickHeight;

	private int ballStartX;
	private int ballStartY;
	private int ballSize;

	private int paddleWidth;
	private int paddleHeight;

	BreakoutModel model;

	@Override
	public void run() {
		model.gameLoop();
	}

	@Override
	public void init() {

		// load configuration
		Properties config = loadProperties("../config/main.config.properties");

		// load game properties depending on configuration and store them in fields
		Properties game = loadProperties("../config/" + config.getProperty("gamePropertiesFile") + ".game.properties");
		initProperties(game);

		// create a model
		model = new BreakoutModel(boundX, boundY, gameSpeed, ballStartX, ballStartY, ballSize, paddleWidth,
				paddleHeight, brickColumns, brickRows, brickOffset, brickHeight);

		// create desktop view
		ViewPC viewPC = new ViewPC(scale);
		model.addView(viewPC);

		// create lighthouse view only when enabled in game properties
		if (Boolean.valueOf(game.getProperty("lighthouse", "true"))) {

			ViewLighthouse viewLighthouse = new ViewLighthouse(config.getProperty("lighthouseUser"),
					config.getProperty("lighthouseToken"));
			model.addView(viewLighthouse);

		}

		// add desktop to graphics program
		add(viewPC.getGameField());

		// controls
		addMouseListeners();
	}

	/**
	 * Initializes game settings and provides fallback values.
	 * 
	 * @param game
	 *            Properties object containing the wanted game settings.
	 */
	private void initProperties(Properties game) {

		// second argument is fallback
		boundX = Integer.valueOf(game.getProperty("boundX", "28"));
		boundY = Integer.valueOf(game.getProperty("boundY", "14"));
		gameSpeed = Integer.valueOf(game.getProperty("gameSpeed", "100"));
		brickRows = Integer.valueOf(game.getProperty("brickRows", "5"));
		brickColumns = Integer.valueOf(game.getProperty("brickColumns", "7"));
		scale = Integer.valueOf(game.getProperty("scale", "20"));
		brickOffset = Integer.valueOf(game.getProperty("brickOffset", "2"));
		brickHeight = Integer.valueOf(game.getProperty("brickHeight", "1"));
		ballStartX = Integer.valueOf(game.getProperty("ballStartX", "4"));
		ballStartY = Integer.valueOf(game.getProperty("ballStartY", "7"));
		ballSize = Integer.valueOf(game.getProperty("ballSize", "1"));
		paddleWidth = Integer.valueOf(game.getProperty("paddleWidth", "3"));
		paddleHeight = Integer.valueOf(game.getProperty("paddleHeight", "1"));
	}

	public void mouseClicked(MouseEvent e) {
		// start game
		model.setGameOver(false);
	}

	public void mouseMoved(MouseEvent e) {
		// maps mouses absolute X position in the ViewPC to the paddle position
		model.getPaddle().setX(e.getX() / scale - model.getPaddle().getWidth() / 2, model.getX());
	}

	/**
	 * Helper method to read properties from a file.
	 * 
	 * @param file
	 *            The file to read from.
	 * @return Properties read from the file.
	 */
	private Properties loadProperties(String file) {
		Properties props = new Properties();
		FileInputStream in = null;

		// read file content
		try {
			in = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return props;
		}

		// parse content to Properties
		try {
			props.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// close input stream
		try {
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return props;
	}
}