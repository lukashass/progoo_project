# Breakout

## Introduction

One important requirement for our game was to always show exactly what the model actually contained.
Thus when playing on the lighthouse the model is supposed to be 28*14 so there is no need for rounding whatsoever.
To accomplish that and still be able to play the game with a higher resolution of the model on desktop only, we used property files.

### How to change settings

In `config/main.config.properties` you can set the `user` and `token` used to connect to the lighthouse.
**Obviously the preset values for `user` and `token` will not work.**
You can also select which `.game.properties` file to load by setting `gamePropertiesFile` to the desired filename.

## Decomposition

### Class

The model of the game consists of a ball, an array of bricks and a paddle.
Each of those elements knows its own position inside of the game field,
so there is no need for a full representation of the game field itself in the model.

The class decomposition could be improved by implementing a superclass
Rectangle with instance variables xPos, yPos, width, height.
Ball would be a subclass of Rectangle with width = height and extra variables for speed.
Brick would be a subclass of Rectangle with an extra variables that store whether or not the brick
has been hit.
Paddle would be a subclass of Rectangle and not of Brick, because it doesn't need the hit variable,
but instead the setX() method that checks for collision with borders of the game field.
This would look something like this:
```
Rectangle
	Ball
	Brick
	Paddle
```
### Method

#### BreakoutModel

Every `gameSpeed` milliseconds `step()` is called, which in turn calls `checkCollision()` for as long as
a collision is detected. Inside `checkCollision()` the method `brickCollision()` is called for each corner of the ball,
as long as there was no collision with a brick detected in the current call to `checkCollision()`.
Lets say the ball moves towards the bottom right. In that case `brickCollision()` calls `brickContaining()` three times,
once for each coordinate next to the corner. `brickContaining()` returns the brick that contains the given coordinates.
```
step()
	checkCollision()
		brickCollision()
			brickContains()
notifyViews()
```

#### ViewLighthouse

In `createView()` the following calls are made:
```
drawPixel() 			// ball
drawWidth() 			// paddle
	width * drawPixel()
bricks * drawWidth()	// bricks
	width * drawPixel()
```
## MVC

### Class hierarchy of the model
```
BreakoutModel
Ball
Brick
	Paddle
```
### Class hierarchy of the view
```
BreakoutView
	ViewLighthouse
	ViewPC
```
### Controller
* included in `Breakout`